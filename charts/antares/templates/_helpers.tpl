{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "antares.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "antares.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "antares.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "antares.labels" -}}
app.kubernetes.io/name: {{ include "antares.name" . }}
helm.sh/chart: {{ include "antares.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{- define "antares.common.matchLabels" -}}
app: {{ template "antares.name" . }}
release: {{ .Release.Name }}
{{- end -}}

{{- define "antares.common.metaLabels" -}}
chart: {{ template "antares.chart" . }}
heritage: {{ .Release.Service }}
{{- end -}}

{{- define "antares.api.labels" -}}
{{ include "antares.api.matchLabels" . }}
{{ include "antares.common.metaLabels" . }}
{{- end -}}

{{- define "antares.api.matchLabels" -}}
component: {{ .Values.api.name | quote }}
{{ include "antares.common.matchLabels" . }}
{{- end -}}

{{- define "antares.api.fullname" -}}
{{- if .Values.api.fullnameOverride -}}
{{- .Values.api.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.api.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.api.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "antares.frontend.labels" -}}
{{ include "antares.frontend.matchLabels" . }}
{{ include "antares.common.metaLabels" . }}
{{- end -}}

{{- define "antares.frontend.matchLabels" -}}
component: {{ .Values.frontend.name | quote }}
{{ include "antares.common.matchLabels" . }}
{{- end -}}

{{- define "antares.frontend.fullname" -}}
{{- if .Values.frontend.fullnameOverride -}}
{{- .Values.frontend.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.frontend.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.frontend.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "antares.rqScheduler.labels" -}}
{{ include "antares.rqScheduler.matchLabels" . }}
{{ include "antares.common.metaLabels" . }}
{{- end -}}

{{- define "antares.rqScheduler.matchLabels" -}}
component: {{ .Values.rqScheduler.name | quote }}
{{ include "antares.common.matchLabels" . }}
{{- end -}}

{{- define "antares.rqScheduler.fullname" -}}
{{- if .Values.rqScheduler.fullnameOverride -}}
{{- .Values.rqScheduler.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.rqScheduler.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.rqScheduler.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "antares.rqWorker.labels" -}}
{{ include "antares.rqWorker.matchLabels" . }}
{{ include "antares.common.metaLabels" . }}
{{- end -}}

{{- define "antares.rqWorker.matchLabels" -}}
component: {{ .Values.rqWorker.name | quote }}
{{ include "antares.common.matchLabels" . }}
{{- end -}}

{{- define "antares.rqWorker.fullname" -}}
{{- if .Values.rqWorker.fullnameOverride -}}
{{- .Values.rqWorker.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.rqWorker.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.rqWorker.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "antares.indexWorker.labels" -}}
{{ include "antares.indexWorker.matchLabels" . }}
{{ include "antares.common.metaLabels" . }}
{{- end -}}

{{- define "antares.indexWorker.matchLabels" -}}
component: {{ .Values.indexWorker.name | quote }}
{{ include "antares.common.matchLabels" . }}
{{- end -}}

{{- define "antares.indexWorker.fullname" -}}
{{- if .Values.indexWorker.fullnameOverride -}}
{{- .Values.indexWorker.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.indexWorker.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.indexWorker.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "antares.pipeline.labels" -}}
{{ include "antares.pipeline.matchLabels" . }}
{{ include "antares.common.metaLabels" . }}
{{- end -}}

{{- define "antares.pipeline.matchLabels" -}}
component: {{ .Values.pipeline.name | quote }}
{{ include "antares.common.matchLabels" . }}
{{- end -}}

{{- define "antares.pipeline.fullname" -}}
{{- if .Values.pipeline.fullnameOverride -}}
{{- .Values.pipeline.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- printf "%s-%s" .Release.Name .Values.pipeline.name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s-%s" .Release.Name $name .Values.pipeline.name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}
